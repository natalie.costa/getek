Funcionalidade: Testar a função de registro

    Contexto: 
        Dado que esteja na pagina de "registro"

    Cenario: Validar a funcionalidade de registrar-se
        Dado que insira "teste2@getek.com" no campo "email"
        E que insira "123456" no campo "senha"
        E que insira "123456" no campo "confirmar senha"
        Quando clicar no botão "Cadastrar-se"
        Então deverá aparecer uma mensagem de progresso "Registrando usuário..."
        E deverá ser exibido uma mensagem "Registrado com sucesso"
