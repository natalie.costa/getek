package com.example.getek;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.google.firebase.auth.FirebaseAuth;

public class telaFaleConosco extends AppCompatActivity implements View.OnClickListener {

    CardView btn_voltar;
    Toolbar toolbaar;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_fale_conosco);
        inicializarComponentes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sair){
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
        if(id == R.id.sobre){
            finish();
            startActivity(new Intent(this, telaSobre.class));
        }
        return true;
    }

    @Override
    public void onClick(View view){
        if(view == btn_voltar){
            startActivity(new Intent(telaFaleConosco.this, telaPrincipal.class));
        }

    }


    private void inicializarComponentes(){
        btn_voltar = (CardView) findViewById(R.id.btn_voltar_tela_principal);
        toolbaar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbaar);
        btn_voltar.setOnClickListener(this);
    }
}
