package com.example.getek;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class telaEsqueciSenha extends AppCompatActivity implements View.OnClickListener {
    CardView voltar, redefinir_senha;
    EditText emailEsqueciSenha;
    Toolbar toolbaar;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_esqueci_senha);

        inicializarComponentes();
    }

    public void redefinirSenha(){
        String email = emailEsqueciSenha.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            Toast.makeText(telaEsqueciSenha.this, "Insira o e-mail", Toast.LENGTH_LONG).show();
        } else{
            progressDialog.setMessage("Enviando configurações...");
            progressDialog.show();
            firebaseAuth.sendPasswordResetEmail(email)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            progressDialog.dismiss();
                            if(task.isSuccessful()){
                                Toast.makeText(telaEsqueciSenha.this, "Redefinição de senha enviada para o email", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(telaEsqueciSenha.this, "E-mail não cadastrado", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }

    @Override
    public void onClick(View view){
        if(view == voltar){
            startActivity(new Intent(this, MainActivity.class));
        } if(view == redefinir_senha){
            redefinirSenha();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sobre){
            finish();
            startActivity(new Intent(this, telaSobre.class));
        } if (id == R.id.idioma){
            finish();
            startActivity(new Intent(this, telaIdioma.class));
        }
        return true;
    }

    private void inicializarComponentes(){
        voltar = (CardView) findViewById(R.id.btn_voltar_login);
        emailEsqueciSenha = (EditText) findViewById(R.id.input_email_esqueci_senha);
        redefinir_senha = (CardView) findViewById(R.id.btn_redefinir_senha);
        toolbaar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbaar);
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        voltar.setOnClickListener(this);
        redefinir_senha.setOnClickListener(this);

    }
}
